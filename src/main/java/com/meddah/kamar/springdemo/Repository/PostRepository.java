package com.meddah.kamar.springdemo.Repository;

import com.meddah.kamar.springdemo.Model.Post;
import com.meddah.kamar.springdemo.Model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface PostRepository extends CrudRepository<Post, UUID> {

    Page<Post> findAll(Pageable pageable);
    Page<Post> findAllByUser(User user, Pageable pageable);
    Post findPostById(UUID id);

}
