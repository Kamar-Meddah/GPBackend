package com.meddah.kamar.springdemo.Repository;

import com.meddah.kamar.springdemo.Model.Mark;
import com.meddah.kamar.springdemo.Model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface MarkRepository extends CrudRepository<Mark,UUID> {

    List<Mark> findMarksBySemesterAndSchoolyearAndGradeAndType(String semester, String schoolyear, String grade, String type);
    List<Mark> findMarksByUser(User user);
    Mark findMarkById(UUID id);
}
