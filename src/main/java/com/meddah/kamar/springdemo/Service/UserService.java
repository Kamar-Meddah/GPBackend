package com.meddah.kamar.springdemo.Service;

import com.meddah.kamar.springdemo.Exception.UserException;
import com.meddah.kamar.springdemo.Model.Post;
import com.meddah.kamar.springdemo.Model.User;
import com.meddah.kamar.springdemo.Repository.UserRepository;
import com.meddah.kamar.springdemo.Security.auth.AuthFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.UUID;

@Service
@Transactional

public class UserService {

    private final UserRepository userRepository;
    private final PostService postService;
    private final NotificationsService notificationsService;
    private final CertificationRequestService certificationRequestService;
    private final MarkService markService;
    private final RenewRegistrationRequestService renewRegistrationRequestService;

    @Autowired
    public UserService(UserRepository userRepository, PostService postService, NotificationsService notificationsService, CertificationRequestService certificationRequestService, MarkService markService, RenewRegistrationRequestService renewRegistrationRequestService) {
        this.userRepository = userRepository;
        this.postService = postService;
        this.notificationsService = notificationsService;
        this.certificationRequestService = certificationRequestService;
        this.markService = markService;
        this.renewRegistrationRequestService = renewRegistrationRequestService;
    }

    public User create(User user) {
        try {
            user.setPassword( this.hash( user.getPassword() ) );
            return this.userRepository.save( user );
        } catch (Exception e) {
            return null;
        }
    }

    public boolean checkUsernameExist(String username) {
        return this.userRepository.findUserByUsername( username ) != null;
    }

    public boolean checkEmailExist(String email) {
        return this.userRepository.findUserByEmail( email ) != null;
    }

    public User update(User user) throws UserException {
        User authenticatedUser = AuthFactory.getUser();
        if (user.getPassword() != null) {
            authenticatedUser.setPassword( this.hash( user.getPassword() ) );
        }
        if (user.getEmail() != null && user.getEmail().matches( "^\\w+([.-]?\\w+)*@\\w+([.-]?\\w+)*(\\.\\w{2,3})+$" )) {
            authenticatedUser.setEmail( user.getEmail() );
        }
        return this.userRepository.save( authenticatedUser );
    }

    private String hash(String password) {
        return BCrypt.hashpw( password, BCrypt.gensalt() );
    }

    public Page<User> getAllPaginated(int page, int perPage) {
        return this.userRepository.findAll( PageRequest.of( page, perPage, Sort.by( Sort.Direction.ASC, "username" ) ) );
    }

    public Page<User> search(String query, int page, int perPage) {
        return this.userRepository.findAllByUsernameLike( '%'+query+'%', PageRequest.of( page, perPage, Sort.by( Sort.Direction.ASC, "username" ) ) );
    }

    public void deleteOne(User user) throws IOException {
        this.notificationsService.deleteNotificationByUser( user );
        this.certificationRequestService.deleteCertificationRequestByUser( user );
        this.renewRegistrationRequestService.safeDeleteRenewRegistrationRequests( user );
        this.markService.safeDeleteMarksbyUser( user );
        for(Post post: user.getPost()) {
            this.postService.deletePost(post);
        }
        this.userRepository.delete(user);
    }

    public void updateOrSaveUser(User user) {
        this.userRepository.save( user );
    }

    public User findUser(String id){
        return this.userRepository.findUserById( UUID.fromString( id ) );
    }


    public User findAdmin(){
        return this.userRepository.findFirstByRole("admin");
    }

    public User findUserByUsername(String username){
        return  this.userRepository.findUserByUsername( username );
    }




}
