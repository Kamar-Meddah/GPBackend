package com.meddah.kamar.springdemo.Service;

import com.meddah.kamar.springdemo.Exception.UserException;
import com.meddah.kamar.springdemo.Model.User;
import com.meddah.kamar.springdemo.Repository.UserRepository;
import name.falgout.jeffrey.testing.junit.mockito.MockitoExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith({SpringExtension.class, MockitoExtension.class})
@SpringBootTest
@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
public class AuthServiceTests {

    private User user;
    private UserRepository userRepository;
    private AuthService authService;

    public AuthServiceTests(@Autowired UserRepository userRepository, @Autowired AuthService authService) {
        this.userRepository = userRepository;
        this.authService = authService;
    }

    @BeforeAll
    void createUser() throws UserException {

        this.user = new User();
        this.user.setEmail( "azeaze@gmail.com" );
        this.user.setRole( "user" );
        this.user.setUsername( "user" );
        this.user.setFirstName( "azaz" );
        this.user.setLastName( "azaz" );
        this.user.setPassword( "azer" );
        this.userRepository.save( this.user );
    }

    @Test
    public void checkUsernameNotExist() {
        User user = this.authService.checkEmailOrUsernameExist( "qsdf" );
        Assertions.assertNull( user, "Username does not Exist" );
    }

    @Test
    public void checkEmailNotExist() {
        User user = this.authService.checkEmailOrUsernameExist( "qsdf@yahoo.fr" );
        Assertions.assertNull( user, "email does not Exist" );
    }

    @Test
    public void checkEmailExist() {
        User user = this.authService.checkEmailOrUsernameExist( "azeaze@gmail.com" );
        Assertions.assertNotNull( user, "email Exist" );
    }

    @Test
    public void checkUsernameExist() {
        User user = this.authService.checkEmailOrUsernameExist( "user" );
        Assertions.assertNotNull( user, "username Exist" );
    }

    @Test
    public void checkInvalidToken() {
        User user = this.authService.checkToken( "fhjh,ui,uiy,uicfcxs" );
        Assertions.assertNull( user, "Throws error when token is invalid" );
    }

}
