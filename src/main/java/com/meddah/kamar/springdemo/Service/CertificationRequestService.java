package com.meddah.kamar.springdemo.Service;

import com.meddah.kamar.springdemo.Exception.CertificationRequestException;
import com.meddah.kamar.springdemo.Model.CertificationRequest;
import com.meddah.kamar.springdemo.Model.User;
import com.meddah.kamar.springdemo.Repository.CertificationRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional

public class CertificationRequestService {

    private final CertificationRequestRepository certificationRequestRepository;

    @Autowired
    public CertificationRequestService(CertificationRequestRepository certificationRequestRepository) {
        this.certificationRequestRepository = certificationRequestRepository;
    }

    public Page<CertificationRequest> getAllNotSentCertificates(int page, int perPage) {
        return this.certificationRequestRepository.getAllBySentIsFalse( PageRequest.of( page, perPage, Sort.by( Sort.Direction.DESC, "createdAt" ) ) );
    }

    public void deleteCertificatRequest(CertificationRequest certificationRequest) {
        this.certificationRequestRepository.delete( certificationRequest );
    }

    public void deleteCertificationRequestByUser(User user) {
        try {
            List<CertificationRequest> certificationRequestList= this.certificationRequestRepository.findCertificationRequestsByUser( user );
            this.certificationRequestRepository.deleteAll( certificationRequestList );
        } catch (Exception ignored) { }
    }

    public CertificationRequest createCertificationRequest(User user, String message, String date) throws CertificationRequestException {
        CertificationRequest certificationRequest = new CertificationRequest();
        certificationRequest.setDate( date );
        certificationRequest.setMessage( message );
        certificationRequest.setUser( user );
        return this.certificationRequestRepository.save( certificationRequest );
    }

    public CertificationRequest findCertifiationRequest(UUID id) {
        return this.certificationRequestRepository.findCertificationRequestById( id );
    }

    public void setSeenCertificationrequest(CertificationRequest certificationrequest) {
        certificationrequest.setSent( true );
        this.certificationRequestRepository.save( certificationrequest );
    }
}
