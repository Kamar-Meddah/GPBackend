package com.meddah.kamar.springdemo.Service;

import com.meddah.kamar.springdemo.Model.Notification;
import com.meddah.kamar.springdemo.Model.User;
import com.meddah.kamar.springdemo.Repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional

public class NotificationsService {

    private final NotificationRepository notificationRepository;

    @Autowired
    public NotificationsService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    public Page<Notification> getAllNotificationsByUser(User user, int page, int perPage) {
        return this.notificationRepository.findNotificationsByUser( user, PageRequest.of( page, perPage, Sort.by( Sort.Direction.DESC, "createdAt" ) ) );
    }

    public Notification getNotificationByIdAndUser(UUID id, User user) {
        return this.notificationRepository.findNotificationsByIdAndUser( id, user );
    }

    public Notification setnotificationSeen(Notification notification) {
        return this.notificationRepository.save( notification );
    }

    public Notification createNotification(String content, User user) {
        Notification notification = new Notification();
        notification.setContent( content );
        notification.setUser( user );
        return this.notificationRepository.save( notification );
    }

    public int getUnseenNotificationsCount(User user) {

        return this.notificationRepository.countNotificationsBySeenAndUser( false, user );
    }

    public void deleteNotificationByUser(User user) {
        try {
            List<Notification> notifications = this.notificationRepository.findNotificationsByUser( user );
            this.notificationRepository.deleteAll( notifications );
        } catch (Exception ignored) { }
    }

}
