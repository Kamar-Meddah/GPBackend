package com.meddah.kamar.springdemo.Repository;

import com.meddah.kamar.springdemo.Model.Notification;
import com.meddah.kamar.springdemo.Model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface NotificationRepository extends CrudRepository<Notification, UUID> {

    Page<Notification> findNotificationsByUser(User user, Pageable pageable);
    List<Notification> findNotificationsByUser(User user);

    Notification findNotificationsByIdAndUser(UUID id, User user);

    int countNotificationsBySeenAndUser(boolean seen, User user);

    void deleteAllByUser(User user);
}
