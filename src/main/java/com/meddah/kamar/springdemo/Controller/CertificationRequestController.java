package com.meddah.kamar.springdemo.Controller;

import com.meddah.kamar.springdemo.Config.BaseConfig;
import com.meddah.kamar.springdemo.Model.CertificationRequest;
import com.meddah.kamar.springdemo.Model.User;
import com.meddah.kamar.springdemo.Security.Annotation.Admin;
import com.meddah.kamar.springdemo.Security.Annotation.Authenticated;
import com.meddah.kamar.springdemo.Security.auth.AuthFactory;
import com.meddah.kamar.springdemo.Service.CertificationRequestService;
import com.meddah.kamar.springdemo.Service.EmailService;
import com.meddah.kamar.springdemo.Service.NotificationsService;
import com.meddah.kamar.springdemo.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping("/api/certification_requests/")
public class CertificationRequestController {

    private final CertificationRequestService certificationRequestService;
    private final BaseConfig baseConfig;
    private final EmailService emailService;
    private final NotificationsService notificationsService;
    private final UserService userService;

    @Autowired
    public CertificationRequestController(CertificationRequestService certificationRequestService, BaseConfig baseConfig, EmailService emailService, NotificationsService notificationsService, UserService userService) {
        this.certificationRequestService = certificationRequestService;
        this.baseConfig = baseConfig;
        this.emailService = emailService;
        this.notificationsService = notificationsService;
        this.userService = userService;
    }

    @GetMapping
    @Authenticated
    @Admin
    public Page<CertificationRequest> getAllCertificateRequest(HttpServletResponse response, @RequestParam("page") String page) throws IOException {
        int p = Integer.parseInt( page ) - 1;
        if (p >= 0) {
            return this.certificationRequestService.getAllNotSentCertificates( p, baseConfig.perPage );
        } else {
            response.sendError( 400, "invalid page input" );
            return null;
        }
    }

    @DeleteMapping("{id}")
    @Authenticated
    @Admin
    public void deleteCertificationRequest(HttpServletResponse response, @PathVariable("id") String id) throws IOException {
        try {
            CertificationRequest certificationRequest = this.certificationRequestService.findCertifiationRequest( UUID.fromString( id ) );
            if (certificationRequest == null) {
                response.sendError( 404 );
            } else {
                this.notificationsService.createNotification( "Your certificate request has been deleted contact the admin for more informations", certificationRequest.getUser() );
                this.certificationRequestService.deleteCertificatRequest( certificationRequest );
            }
        } catch (Exception ignored) {
            response.sendError( 404, "No certification request was found" );
        }
    }

    @PatchMapping("{id}")
    @Authenticated
    @Admin
    public void sendCertification(HttpServletResponse response, @RequestParam("file") MultipartFile file, @PathVariable("id") String id) throws IOException {
        if (Objects.equals( file.getContentType(), "application/pdf" )) {
            CertificationRequest certificationRequest = this.certificationRequestService.findCertifiationRequest( UUID.fromString( id ) );
            if (certificationRequest != null) {
                boolean success = this.emailService.sendEmail(
                        certificationRequest.getUser().getEmail(),
                        "certfication request",
                        String.join( "\n",
                                     "Hello " + certificationRequest.getUser().getUsername(),
                                     "we have received your certification request",
                                     "it is attached to this mail download it"
                        ),
                        String.join( "\n",
                                     "<h1>" + "Hello " + certificationRequest.getUser().getUsername() + "</h1>",
                                     "<p>" + "we have received your certification request",
                                     "it is attached to this mail download it" + "</p>"
                        ),
                        certificationRequest.getUser().getFirstName() + "_" + certificationRequest.getUser().getLastName() + "_diploma.pdf",
                        file
                );
                if (success) {
                    this.certificationRequestService.setSeenCertificationrequest( certificationRequest );
                    this.notificationsService.createNotification( "We have seen your certificate request and we have sent it check your email", certificationRequest.getUser() );
                    response.setStatus( 201 );
                }else{
                    response.sendError( 400, "an Error occured can not send email" );
                }
            } else {
                response.sendError( 404 );
            }
        } else {
            response.sendError( 406, "Only PDF are allowed" );
        }
    }

    @PostMapping
    @Authenticated
    public void createCertificateRequest(HttpServletResponse response, @RequestBody CertificationRequest certificationRequest) throws IOException {
        try {
            User user = AuthFactory.getUser();
            this.certificationRequestService.createCertificationRequest( user, certificationRequest.getMessage(), certificationRequest.getDate() );
            this.notificationsService.createNotification( "You have a new Certificate request from '" + user.getFullName() + "'", this.userService.findAdmin() );
            response.setStatus( 201 );
        } catch (Exception ignored) {
            response.sendError( 400, "an error occured" );
        }
    }
}
