package com.meddah.kamar.springdemo.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.meddah.kamar.springdemo.Exception.MarkException;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "marks")
public class Mark {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private UUID id;

    @Column(name = "semester", nullable = false)
    private String semester;

    @JsonIgnore
    @Column(name = "path", nullable = false)
    private String path = "educational_managment/marks/";

    @Column(name = "file_name", unique = true)
    private String fileName;

    @Column(name = "school_year", nullable = false)
    private String schoolyear;

    @Column(name = "grade", nullable = false)
    private String grade;

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "created_at", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt;

    @Column(name = "updated_at", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updatedAt;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;


    public Mark() {
    }

    public void setSchoolyear(String schoolyear) throws MarkException {
        if (schoolyear.matches( "^(20)\\d{2}/(20)\\d{2}$" )) {
            this.schoolyear = schoolyear;
        } else {
            throw new MarkException( "school year must be of format yyyy/yyyy" );
        }
    }

    public void setUser(User user) throws MarkException {
        if (Objects.equals( user.getRole(), "teacher" )) {
            this.user = user;
        } else {
            throw new MarkException( "The user must be a teacher" );
        }
    }


}
