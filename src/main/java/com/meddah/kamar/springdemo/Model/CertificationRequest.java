package com.meddah.kamar.springdemo.Model;

import com.meddah.kamar.springdemo.Exception.CertificationRequestException;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "certification_request")
@Getter
@Setter
public class CertificationRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private UUID id;

    @Column(name = "message")
    private String message;

    @Column(name = "date")
    private String date;

    @Column(name = "sent")
    private boolean sent = false;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "created_at", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt;

    @Column(name = "updated_at", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updatedAt;

    public CertificationRequest(String message, String date, User user) {
        this.message = message;
        this.date = date;
        this.user = user;
    }

    public CertificationRequest() {
    }

    public void setDate(String date) throws CertificationRequestException {
        if (date.matches( "^(20)\\d{2}$" )) {
            this.date = date;
        } else {
            throw new CertificationRequestException( "a date must be of format yyyy and between 2000 and 2099" );
        }
    }


}
