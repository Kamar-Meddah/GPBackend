package com.meddah.kamar.springdemo.Service;

import com.meddah.kamar.springdemo.Config.BaseConfig;
import com.meddah.kamar.springdemo.Exception.MarkException;
import com.meddah.kamar.springdemo.Model.Mark;
import com.meddah.kamar.springdemo.Model.User;
import com.meddah.kamar.springdemo.Repository.MarkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Service
@Transactional

public class MarkService {

    private final MarkRepository markRepository;
    private final BaseConfig baseConfig;

    @Autowired
    public MarkService(MarkRepository markRepository, BaseConfig baseConfig) {
        this.markRepository = markRepository;
        this.baseConfig = baseConfig;
    }

    public Mark createMark(String schoolYear, String semester, String name, String grade, String type, User user) throws MarkException {
        Mark mark = new Mark();
        mark.setGrade( grade );
        mark.setSchoolyear( schoolYear );
        mark.setSemester( semester );
        mark.setUser( user );
        mark.setType( type );
        this.markRepository.save( mark );
        mark.setFileName( name + "-" + mark.getId().toString().replace( "-", "" ) + ".pdf" );
        return this.markRepository.save( mark );
    }

    public List<Mark> findMarksBySemesterAndSchoolyearAndGradeAndType(String semester, String schoolyear, String grade, String type) {
        return this.markRepository.findMarksBySemesterAndSchoolyearAndGradeAndType( semester, schoolyear, grade, type );
    }

    public Mark findMark(String id) {
        return this.markRepository.findMarkById( UUID.fromString( id ) );
    }

    public void safeDeleteMarksbyUser(User user) {
        try {
            List<Mark> marks = this.markRepository.findMarksByUser( user );
            for (Mark mark : marks) {
                Files.deleteIfExists( Paths.get( baseConfig.upload, mark.getPath(), mark.getFileName()));
                this.markRepository.delete( mark );
            }
        } catch (Exception ignored) {
        }
    }
}
