package com.meddah.kamar.springdemo.Repository;

import com.meddah.kamar.springdemo.Model.CertificationRequest;
import com.meddah.kamar.springdemo.Model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface CertificationRequestRepository extends CrudRepository<CertificationRequest, UUID> {

    Page<CertificationRequest> getAllBySentIsFalse(Pageable pageable);
    List<CertificationRequest> findCertificationRequestsByUser(User user);
    CertificationRequest findCertificationRequestById(UUID id);
    void deleteAllByUser(User user);
}
