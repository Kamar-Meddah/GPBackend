package com.meddah.kamar.springdemo.Service;

import com.meddah.kamar.springdemo.Exception.CertificationRequestException;
import com.meddah.kamar.springdemo.Exception.UserException;
import com.meddah.kamar.springdemo.Model.CertificationRequest;
import com.meddah.kamar.springdemo.Model.User;
import name.falgout.jeffrey.testing.junit.mockito.MockitoExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith({SpringExtension.class, MockitoExtension.class})
@SpringBootTest
@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
public class CertificationRequestServiceTest {

    @Autowired
    private CertificationRequestService certificationRequestService;
    @Autowired
    private UserService userService;
    private User user;

    @BeforeAll
    public void createUser() throws UserException {
        this.user = new User();
        this.user.setEmail( "azeaze@gmail.com" );
        this.user.setRole( "user" );
        this.user.setUsername( "user" );
        this.user.setFirstName( "azaz" );
        this.user.setLastName( "azaz" );
        this.user.setPassword( "azer" );
        this.user = this.userService.create( this.user );
    }

    @Test
    public void testCreateCertificationRequest() throws CertificationRequestException {
        CertificationRequest certificationRequest =
                this.certificationRequestService.createCertificationRequest( this.user, "testest", "2018" );
        Assertions.assertNotNull( certificationRequest );
    }




}
