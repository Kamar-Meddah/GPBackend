package com.meddah.kamar.springdemo.Service;

import com.meddah.kamar.springdemo.Config.BaseConfig;
import com.meddah.kamar.springdemo.Model.Image;
import com.meddah.kamar.springdemo.Model.Post;
import com.meddah.kamar.springdemo.Repository.ImageRepository;
import com.meddah.kamar.springdemo.Security.Annotation.Authenticated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Service
@Transactional

public class ImageService {

    private final ImageRepository imageRepository;
    private final BaseConfig baseConfig;

    @Autowired
    public ImageService(ImageRepository imageRepository, BaseConfig baseConfig) {
        this.imageRepository = imageRepository;
        this.baseConfig = baseConfig;
    }

    public Image create(Post post){
        Image image = new Image();
        image.setPost( post );
        image = this.imageRepository.save( image );
        image.setName( image.getId()+".jpg" );
        return this.imageRepository.save( image );
    }

    public Image findImage(String id){
        return this.imageRepository.findAllById(UUID.fromString(id ));
    }

    public void deleteImage(Image image) throws IOException {
        Files.deleteIfExists( Paths.get(baseConfig.upload, image.getPath(),image.getName()));
        this.imageRepository.delete( image );
    }

}
