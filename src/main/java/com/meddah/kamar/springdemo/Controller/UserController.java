package com.meddah.kamar.springdemo.Controller;

import com.meddah.kamar.springdemo.Config.BaseConfig;
import com.meddah.kamar.springdemo.Exception.UserException;
import com.meddah.kamar.springdemo.Model.User;
import com.meddah.kamar.springdemo.Security.Annotation.Admin;
import com.meddah.kamar.springdemo.Security.Annotation.Authenticated;
import com.meddah.kamar.springdemo.Security.auth.AuthFactory;
import com.meddah.kamar.springdemo.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping("api/user/")
public class UserController {

    private final UserService userService;
    private final AuthService authService;
    private final EmailService emailService;
    private final BaseConfig baseConfig;
    private final NotificationsService notificationsService;


    @Autowired
    public UserController(UserService userService, AuthService authService, EmailService emailService, NotificationsService notificationsService, BaseConfig baseConfig, CertificationRequestService certificationRequestService) {
        this.userService = userService;
        this.authService = authService;
        this.emailService = emailService;
        this.notificationsService = notificationsService;
        this.baseConfig = baseConfig;
    }

    @PostMapping
    public void createUser(HttpServletResponse response, @RequestBody User user) throws IOException {
        if (this.userService.checkEmailExist( user.getEmail() )) {
            response.sendError( 406, "Email already exist" );
        } else if (this.userService.checkUsernameExist( user.getUsername() )) {
            response.sendError( 406, "username already exist" );
        } else {
            if (this.userService.findAdmin() == null) {
                user.setConfirmationToken( null );
                user.setRole( "admin" );
                this.emailService.sendEmail( user.getEmail(), "Welcome", String.join( "\n", "Hello " + user.getUsername(), "thx for your registration" ), String.join( "\n", "<h1 style='color: firebrick'>Hello " + user.getUsername() + "</h1>", "<p>Thx for your registration</p>" ) );
            } else {
                user.setConfirmationToken( UUID.randomUUID().toString().replace( "-", "" ) );
                this.notificationsService.createNotification( "A new user with the name '" + user.getFullName() + "' have been registered and waiting for confirmation", this.userService.findAdmin() );
            }
            User newUser = this.userService.create( user );
            response.setStatus( 201 );
        }
    }

    @PutMapping("{id}")
    @Authenticated
    public void updateUser(HttpServletResponse response, @RequestBody Map<String, String> input, @PathVariable("id") String id) throws IOException, UserException {
        if (Objects.equals( AuthFactory.getUser().getId().toString(), id )) {
            User user = new User( input.get( "email" ), input.get( "password" ) );
            // password update
            if (user.getPassword() != null) {
                if (this.authService.checkPassword( input.get( "oldPassword" ), AuthFactory.getUser().getPassword() )) {
                    this.userService.update( user );
                    response.setStatus( 201 );
                } else {
                    response.sendError( 406, "Wrong Password" );
                }
            }
            // end password update

            //  Email Update
            if (user.getEmail() != null) {
                if (this.authService.checkEmailOrUsernameExist( user.getEmail() ) == null) {
                    this.userService.update( user );
                    response.setStatus( 201 );
                } else {
                    response.sendError( 406, "Email already exist" );
                }
            }
            //  End Email Update

        } else {
            response.sendError( 401 );
        }
    }

    @GetMapping
    @Authenticated
    @Admin
    public Page<User> index(HttpServletResponse response, @RequestParam(value = "page") String page, @RequestParam(value = "query", required = false) String query) throws IOException {
        int p = Integer.parseInt( page ) - 1;
        if (p >= 0) {
            if (query == null) {
                return this.userService.getAllPaginated( p, baseConfig.perPage );
            } else {
                return this.userService.search( query, p, baseConfig.perPage );
            }
        } else {
            response.sendError( 400, "invalid page input" );
            return null;
        }
    }

    @DeleteMapping("{id}")
    @Authenticated
    @Admin
    public void deleteOne(HttpServletResponse response, @PathVariable("id") String id) throws IOException {
        User user = this.userService.findUser( id );
        if (user != null) {
            this.userService.deleteOne( user );
        } else {
            response.sendError( 404 );
        }
    }

    @PatchMapping("{id}")
    @Authenticated
    @Admin
    public void patch(@PathVariable("id") String id, @RequestBody Map inputData, HttpServletResponse response) throws IOException {
        if (Objects.equals( inputData.get( "role" ), "admin" ) || Objects.equals( inputData.get( "role" ), "student" ) || Objects.equals( inputData.get( "role" ), "teacher" )) {
            try {
                User user = this.userService.findUser( id );
                if (user != null) {
                    user.setRole( (String) inputData.get( "role" ) );
                    user.setConfirmationToken( null );
                    this.userService.updateOrSaveUser( user );
                    this.emailService.sendEmail( user.getEmail(), "Welcome", String.join( "\n", "Hello " + user.getUsername(), "thx for your registration" ), String.join( "\n", "<h1 style='color: firebrick'>Hello " + user.getUsername() + "</h1>", "<p>Thx for your registration</p>" ) );
                    this.notificationsService.createNotification( "You have a been confirmed and assigned to '" + user.getRole() + "'", user );
                    ;
                } else {
                    response.sendError( 404, "no user was found" );
                }
                response.setStatus( 201 );
            } catch (Exception e) {
                response.sendError( 400, "Invalid id" );
            }
        } else {
            response.sendError( 400, "Invalid role property" );
        }
    }

    @GetMapping("{username}")
    public User getUser(HttpServletResponse response, @PathVariable("username") String username) throws IOException {

        User user = this.userService.findUserByUsername( username );
        if (user == null || user.getRole() == null) {
            response.sendError( 404, "Student do not exist" );
        }
        return user;
    }
}
