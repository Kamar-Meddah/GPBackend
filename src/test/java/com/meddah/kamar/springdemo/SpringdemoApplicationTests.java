package com.meddah.kamar.springdemo;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest
public class SpringdemoApplicationTests {

    @Test
    public void contextLoads() {
        Assert.assertEquals( true,true );
    }

}
