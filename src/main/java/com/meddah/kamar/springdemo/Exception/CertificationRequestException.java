package com.meddah.kamar.springdemo.Exception;

public class CertificationRequestException extends Exception {

    public CertificationRequestException() {
    }

    public CertificationRequestException(String message) {
        super( message );
    }
}
