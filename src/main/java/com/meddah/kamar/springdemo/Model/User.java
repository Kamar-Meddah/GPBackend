package com.meddah.kamar.springdemo.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.meddah.kamar.springdemo.Exception.UserException;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

@Entity
@Table(name = "users")

@Data
public class User {
    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "role")
    private String role;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name = "reset_token", unique = true)
    private String resetToken;

    @JsonIgnore
    @Column(name = "remember_token", unique = true)
    @Size(max = 305)
    private String rememberToken;

    @Column(name = "confirmation_token", unique = true)
    private String confirmationToken;

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private Set<Post> post = new TreeSet<>();

    @Column(name = "created_at", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt;

    @JsonIgnore
    @Column(name = "updated_at", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updatedAt;


    public User() {
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }


    public void setEmail(String email) throws UserException {
        if (!((email.matches( "^\\w+([.-]?\\w+)*@\\w+([.-]?\\w+)*(\\.\\w{2,3})+$" )))) {
            throw new UserException( "Invalid Email" );
        }
        this.email = email;
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }


    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                ", resetToken='" + resetToken + '\'' +
                ", rememberToken='" + rememberToken + '\'' +
                ", confirmationToken='" + confirmationToken + '\'' +
                '}';
    }

}
