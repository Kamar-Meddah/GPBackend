package com.meddah.kamar.springdemo.Model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class RenewRegistrationRequestTests {

    @Test
    public void testPathIsCorrect() {
        RenewRegistrationRequest renewRegistrationRequest = new RenewRegistrationRequest();
        Assertions.assertEquals( renewRegistrationRequest.getPath(), "educational_managment/renew_registration_request/" );
    }


}
