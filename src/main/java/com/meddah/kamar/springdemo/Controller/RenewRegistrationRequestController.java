package com.meddah.kamar.springdemo.Controller;

import com.meddah.kamar.springdemo.Config.BaseConfig;
import com.meddah.kamar.springdemo.Model.RenewRegistrationRequest;
import com.meddah.kamar.springdemo.Model.User;
import com.meddah.kamar.springdemo.Security.Annotation.Admin;
import com.meddah.kamar.springdemo.Security.Annotation.Authenticated;
import com.meddah.kamar.springdemo.Security.Annotation.Student;
import com.meddah.kamar.springdemo.Security.auth.AuthFactory;
import com.meddah.kamar.springdemo.Service.NotificationsService;
import com.meddah.kamar.springdemo.Service.RenewRegistrationRequestService;
import com.meddah.kamar.springdemo.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@RestController
@RequestMapping("/api/renew_registration_request/")
public class RenewRegistrationRequestController {

    private final RenewRegistrationRequestService renewRegistrationRequestService;
    private final BaseConfig baseConfig;
    private final NotificationsService notificationsService;
    private final UserService userService;

    @Autowired
    public RenewRegistrationRequestController(RenewRegistrationRequestService renewRegistrationRequestService, BaseConfig baseConfig, NotificationsService notificationsService, UserService userService) {
        this.renewRegistrationRequestService = renewRegistrationRequestService;
        this.baseConfig = baseConfig;
        this.notificationsService = notificationsService;
        this.userService = userService;
    }

    @GetMapping
    @Authenticated
    @Admin
    public Page<RenewRegistrationRequest> getAllRenewRegistrationRequest(HttpServletResponse response, @RequestParam("page") String page) throws IOException {
        int p = Integer.parseInt( page ) - 1;
        if (p >= 0) {
            return this.renewRegistrationRequestService.getAllRenewRegistrationRequest( p, baseConfig.perPage );
        } else {
            response.sendError( HttpServletResponse.SC_BAD_REQUEST, "invalid page input" );
            return null;
        }
    }

    @PostMapping
    @Authenticated
    @Student
    public void createRenewRegistrationRequest(HttpServletResponse response, @RequestParam("file") MultipartFile file, @RequestParam("grade") String grade) throws IOException {
        try {
            if (Objects.equals( file.getContentType().split( "/" )[0], "image" )) {
                User user = AuthFactory.getUser();
                RenewRegistrationRequest renewRegistrationRequest = this.renewRegistrationRequestService.createRenewRegistrationRequest( grade, user );
                Path path = Paths.get( baseConfig.upload, renewRegistrationRequest.getPath(), renewRegistrationRequest.getName() );
                Files.createDirectories( path.getParent() );
                Files.newOutputStream( path ).write( file.getBytes() );
                this.notificationsService.createNotification( "You have a new Renew registration  request from '" + user.getFullName() + "'", this.userService.findAdmin() );

                response.setStatus( HttpServletResponse.SC_CREATED );
            } else {
                response.sendError( 406, "Only images are allowed" );
            }

        } catch (Exception ignored) {
            response.sendError( 406, "Request already exist" );
        }
    }


    @GetMapping(value = "{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getRenewRegistrationImage(HttpServletResponse response, @PathVariable("id") String id) throws IOException {
        RenewRegistrationRequest renewRegistrationRequest = this.renewRegistrationRequestService.findRenewRegistrationRequest( id );
        if (renewRegistrationRequest == null) {
            response.sendError( 404 );
            return null;
        } else {
            Path path = Paths.get( baseConfig.upload, renewRegistrationRequest.getPath(), renewRegistrationRequest.getName() );
            InputStream inputStream = Files.newInputStream( path );
            return inputStream.readAllBytes();
        }
    }

    @DeleteMapping("{id}")
    @Authenticated
    @Admin
    public void deleteRenewRegistrationRequest(HttpServletResponse response, @PathVariable("id") String id) throws IOException {
        RenewRegistrationRequest renewRegistrationRequest = this.renewRegistrationRequestService.findRenewRegistrationRequest( id );
        if (renewRegistrationRequest != null) {
            try {
                this.notificationsService.createNotification( "Your renew registration request has been deleted try again", renewRegistrationRequest.getUser() );
                this.renewRegistrationRequestService.deleteRegistrationRequest( renewRegistrationRequest, baseConfig );
                response.setStatus( 201 );
            } catch (Exception ignored) {
                response.sendError( 400, "An Error occurred try again" );
            }

        } else {
            response.sendError( 404, "No Renew Registration Request Found" );
        }
    }

    @PatchMapping("{id}")
    @Authenticated
    @Admin
    public void patchRenewRegistrationRequest(HttpServletResponse response, @PathVariable("id") String id) throws IOException {
        RenewRegistrationRequest renewRegistrationRequest = this.renewRegistrationRequestService.findRenewRegistrationRequest( id );
        if (renewRegistrationRequest == null) {
            response.sendError( 404, "not found" );
        } else {
            renewRegistrationRequest.setApproved( true );
            this.renewRegistrationRequestService.updateRenewRegistrationRequest( renewRegistrationRequest );
            this.notificationsService.createNotification( "Your renew registration was accepted ", renewRegistrationRequest.getUser() );
            response.setStatus( 201 );
        }
    }

}
