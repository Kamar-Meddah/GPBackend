package com.meddah.kamar.springdemo.Repository;

import com.meddah.kamar.springdemo.Model.RenewRegistrationRequest;
import com.meddah.kamar.springdemo.Model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface RenewRegistrationRequestRepository extends CrudRepository<RenewRegistrationRequest,UUID> {

    Page<RenewRegistrationRequest> findAllByApprovedIsFalse(Pageable pageable);

    RenewRegistrationRequest findRenewRegistrationRequestById(UUID id);
    List<RenewRegistrationRequest> findRenewRegistrationRequestByUser(User user);
}
