package com.meddah.kamar.springdemo.Exception;

public class MarkException extends Exception {

    public MarkException() {
    }

    public MarkException(String message) {
        super( message );
    }
}
