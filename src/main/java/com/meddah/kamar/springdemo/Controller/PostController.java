package com.meddah.kamar.springdemo.Controller;

import com.meddah.kamar.springdemo.Config.BaseConfig;
import com.meddah.kamar.springdemo.Model.Image;
import com.meddah.kamar.springdemo.Model.Post;
import com.meddah.kamar.springdemo.Model.User;
import com.meddah.kamar.springdemo.Security.Annotation.AdminOrTeacher;
import com.meddah.kamar.springdemo.Security.Annotation.Authenticated;
import com.meddah.kamar.springdemo.Security.auth.AuthFactory;
import com.meddah.kamar.springdemo.Service.ImageService;
import com.meddah.kamar.springdemo.Service.PostService;
import com.meddah.kamar.springdemo.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping("/api/post/")
public class PostController {

    private final BaseConfig baseConfig;
    private final ImageService imageService;
    private final PostService postService;
    private final UserService userService;

    @Autowired
    public PostController(PostService postService, BaseConfig baseConfig, ImageService imageService, UserService userService) {
        this.postService = postService;
        this.baseConfig = baseConfig;
        this.imageService = imageService;
        this.userService = userService;
    }


    @PostMapping
    @Authenticated
    @AdminOrTeacher
    public void createPost(@RequestParam("content") String content, HttpServletResponse response, @RequestParam("files") MultipartFile[] files) throws IOException {
        Post post = this.postService.create(content, AuthFactory.getUser());
        for (MultipartFile file : files) {
            if (!file.isEmpty()) {
                try {
                    byte[] bytes = file.getBytes();
                    Image image = this.imageService.create(post);
                    Path path = Paths.get(baseConfig.upload, image.getPath(), image.getName());
                    Files.createDirectories(path.getParent());
                    Files.newOutputStream(path).write(bytes);
                } catch (IOException ignored) {
                    response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, "An error occurred while uploading files");
                }
            }
        }
        if (post != null) {
            response.setStatus(201);
        } else {
            response.sendError(400, "Can not create the post");
        }

    }


    @GetMapping
    @Authenticated
    public Page<Post> getPosts(HttpServletResponse response, @RequestParam("page") String page, @RequestParam(value = "query", required = false) String query) throws IOException {
        int p = Integer.parseInt(page) - 1;
        if (p >= 0) {
            if (query == null || query.isEmpty()) {
                return this.postService.getAllPosts(p, this.baseConfig.perPage);
            } else {
                User user = this.userService.findUserByUsername(query);
                if (user == null) {
                    response.sendError(403, "user not found");
                } else {
                    return this.postService.getAllPosts(p, this.baseConfig.perPage, query, user);
                }
            }
        } else {
            response.sendError(400, "invalid page input");
        }
        return null;
    }

    @GetMapping("{id}")
    @Authenticated
    public Post getPost(HttpServletResponse response, @PathVariable("id") String id) throws IOException {
        Post post = this.postService.findPost(id);
        if (post == null) {
            response.sendError(404, "post not found");
        }
        return post;
    }

    @DeleteMapping("{id}")
    @Authenticated
    @AdminOrTeacher
    public void deletePost(HttpServletResponse response, @PathVariable("id") String id) throws IOException {
        Post post = this.postService.findPost(id);
        User user = AuthFactory.getUser();
        if (post != null) {
            if (user.getId() == post.getUser().getId()) {
                this.postService.deletePost(post);
                response.setStatus(HttpServletResponse.SC_CREATED);
            } else {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            }

        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "Post not found");
        }
    }

    @PatchMapping("{id}")
    @Authenticated
    @AdminOrTeacher
    public void updatePostContent(HttpServletResponse response, @PathVariable("id") String id, @RequestBody Post requestPost) throws IOException {
        Post post = this.postService.findPost(id);
        User user = AuthFactory.getUser();
        if (post != null) {
            if (user.getId() == post.getUser().getId()) {
                post.setContent(requestPost.getContent());
                this.postService.update(post);
                response.setStatus(HttpServletResponse.SC_CREATED);
            } else {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            }
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "Post not found");
        }
    }

}
