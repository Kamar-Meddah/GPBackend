package com.meddah.kamar.springdemo.Config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BaseConfig {

    @Value("${Jwt_secret}")
    public String jwtSecret;

    @Value("${storage_upload}")
    public String upload;

    @Value("${Jwt_expiration}")
    public int jwtExp;

    @Value("${Element_per_page}")
    public int perPage;

}
