package com.meddah.kamar.springdemo.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(
        name = "renew_registration_request",
        uniqueConstraints = @UniqueConstraint(columnNames = {"user_id", "school_year"})
)
public class RenewRegistrationRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private UUID id;

    @JsonIgnore
    @Column(name = "name", unique = true)
    private String name;

    @JsonIgnore
    @Column(name = "path", nullable = false)
    private String path = "educational_managment/renew_registration_request/";

    @Column(name = "school_year", nullable = false)
    private String schoolYear;

    @Column(name = "grade", nullable = false)
    private String grade;


    @Column(name = "approved", nullable = false)
    private boolean approved = false;

    @Column(name = "created_at", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt;

    @Column(name = "updated_at", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updatedAt;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    public RenewRegistrationRequest() {
    }

    public RenewRegistrationRequest(String name, String schoolYear, String grade, boolean approved, User user) {
        this.name = name;
        this.schoolYear = schoolYear;
        this.grade = grade;
        this.approved = approved;
        this.user = user;
    }
}
