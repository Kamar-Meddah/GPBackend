package com.meddah.kamar.springdemo.Model;

import com.meddah.kamar.springdemo.Exception.MarkException;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MarkTests {


    private String path = "educational_managment/marks/";

    @Test
    public void testMarkPath() {
        Mark mark = new Mark();
        Assertions.assertEquals( mark.getPath(), "educational_managment/marks/", "Check if path is correct" );
    }

    @Test
    public void testCorrectSchoolYear() throws MarkException {
        Mark mark = new Mark();
        mark.setSchoolyear( "2017/2018" );
        Assertions.assertEquals( mark.getSchoolyear(), "2017/2018", "School year is correct" );
    }

    @Test
    public void testInCorrectSchoolYear() throws MarkException {
        Mark mark = new Mark();
        Assertions.assertThrows( MarkException.class, () -> {
            mark.setSchoolyear( "9085/1024" );
        },"throw error when schoolYear is incorrect" );
    }

    @Test
    public void testUserHasRoleTeacher() throws MarkException {
        User user = new User( );
        user.setRole( "teacher" );
        Mark mark = new Mark();
        mark.setUser( user );
        Assertions.assertTrue( true,"User must have teacher role" );
    }

    @Test
    public void testUserHasNotRoleTeacher() throws MarkException {
        User user = new User( );
        user.setRole( "admin" );
        Mark mark = new Mark();
        Assertions.assertThrows( MarkException.class,() -> {
            mark.setUser( user );
        },"User must have teacher role" );
    }




}
