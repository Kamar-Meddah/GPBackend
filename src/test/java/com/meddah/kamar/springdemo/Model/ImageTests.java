package com.meddah.kamar.springdemo.Model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@SpringBootTest
public class ImageTests {

  @Test
  public void testImagePath(){
      Image image = new Image();
      Assertions.assertEquals( image.getPath(),"post/image/", "Check if path is correct" );
  }

}
