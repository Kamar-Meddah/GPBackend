package com.meddah.kamar.springdemo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.Date;

@Service
@Transactional

public class EmailService {

    private final JavaMailSender emailSender;

    @Autowired
    public EmailService(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    public boolean sendEmail(String to, String subject, String text, String html, String jointName, MultipartFile joint) {
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper( message, true );
            helper.setTo( to );
            helper.setSubject( subject );
            helper.setFrom( "demoSpring@demo.org" );
            helper.setSentDate( Date.from( Instant.now() ) );
            helper.setText( text, html );
            helper.addAttachment( jointName, joint::getInputStream );
            this.emailSender.send( message );
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean sendEmail(String to, String subject, String text) {
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper( message, false );
            helper.setTo( to );
            helper.setSubject( subject );
            helper.setFrom( "demoSpring@demo.org" );
            helper.setSentDate( Date.from( Instant.now() ) );
            helper.setText( text );
            this.emailSender.send( message );
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean sendEmail(String to, String subject, String text, String html) {
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper( message, true );
            helper.setTo( to );
            helper.setSubject( subject );
            helper.setFrom( "demoSpring@demo.org" );
            helper.setSentDate( Date.from( Instant.now() ) );
            helper.setText( text, html );
            this.emailSender.send( message );
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
