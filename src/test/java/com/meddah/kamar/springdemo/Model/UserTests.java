package com.meddah.kamar.springdemo.Model;

import com.meddah.kamar.springdemo.Exception.UserException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class UserTests {

    @Test
    public void testCorrectEmail() throws UserException {
        User user = new User();
        user.setEmail( "aaa@gaaa.com" );
        Assertions.assertTrue( true, "Check email is correct" );
    }

    @Test
    public void testInCorrectEmail() throws UserException {
        User user = new User();
        Assertions.assertThrows( UserException.class, () -> {
            user.setEmail( "aaa@fghjkl.azertyuiop" );
        }, "throws error when email is incorrect" );
    }

}
