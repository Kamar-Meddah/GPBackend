package com.meddah.kamar.springdemo.Repository;

import com.meddah.kamar.springdemo.Model.Image;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ImageRepository extends CrudRepository<Image, UUID> {

    Image findAllById(UUID uuid);
}
