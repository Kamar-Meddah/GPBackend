package com.meddah.kamar.springdemo.Controller;

import com.meddah.kamar.springdemo.Config.BaseConfig;
import com.meddah.kamar.springdemo.Model.Image;
import com.meddah.kamar.springdemo.Service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

@RestController
@RequestMapping("/image/")
public class ImageController {

    private final BaseConfig baseConfig;
    private final ImageService imageService;

    @Autowired
    public ImageController(BaseConfig baseConfig, ImageService imageService) {
        this.baseConfig = baseConfig;
        this.imageService = imageService;
    }


    @GetMapping(value = "{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getImage(HttpServletResponse response, @PathVariable("id") String id) throws IOException {
        Image image = this.imageService.findImage( id );
        if (image != null) {
            InputStream inputStream = Files.newInputStream( Paths.get( baseConfig.upload, image.getPath(), image.getName() ) );
            return inputStream.readAllBytes();
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

}
