package com.meddah.kamar.springdemo.Service;

import com.meddah.kamar.springdemo.Config.BaseConfig;
import com.meddah.kamar.springdemo.Model.RenewRegistrationRequest;
import com.meddah.kamar.springdemo.Model.User;
import com.meddah.kamar.springdemo.Repository.RenewRegistrationRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Year;
import java.util.List;
import java.util.UUID;

@Service
@Transactional

public class RenewRegistrationRequestService {

    private final RenewRegistrationRequestRepository renewRegistrationRequestRepository;
    private final BaseConfig baseConfig;

    @Autowired
    public RenewRegistrationRequestService(RenewRegistrationRequestRepository renewRegistrationRequestRepository, BaseConfig baseConfig) {
        this.renewRegistrationRequestRepository = renewRegistrationRequestRepository;
        this.baseConfig = baseConfig;
    }

    public Page<RenewRegistrationRequest> getAllRenewRegistrationRequest(int page, int perPage) {
        return this.renewRegistrationRequestRepository.findAllByApprovedIsFalse( PageRequest.of( page, perPage, Sort.by( Sort.Direction.ASC, "createdAt" ) ) );
    }

    public RenewRegistrationRequest findRenewRegistrationRequest(String id) {
        return this.renewRegistrationRequestRepository.findRenewRegistrationRequestById( UUID.fromString( id ) );
    }

    public void deleteRegistrationRequest(RenewRegistrationRequest renewRegistrationRequest, BaseConfig baseConfig) throws IOException {
        Files.deleteIfExists( Paths.get( baseConfig.upload, renewRegistrationRequest.getPath(), renewRegistrationRequest.getName() ) );
        this.renewRegistrationRequestRepository.delete( renewRegistrationRequest );
    }

    public RenewRegistrationRequest updateRenewRegistrationRequest(RenewRegistrationRequest renewRegistrationRequest) {
        return this.renewRegistrationRequestRepository.save( renewRegistrationRequest );
    }

    public RenewRegistrationRequest createRenewRegistrationRequest(String grade, User user) {
        RenewRegistrationRequest renewRegistrationRequest = new RenewRegistrationRequest();
        renewRegistrationRequest.setGrade( grade );
        renewRegistrationRequest.setSchoolYear( Year.now().toString() );
        renewRegistrationRequest.setUser( user );
        renewRegistrationRequest = this.renewRegistrationRequestRepository.save( renewRegistrationRequest );
        renewRegistrationRequest.setName( renewRegistrationRequest.getId() + ".jpg" );
        return this.renewRegistrationRequestRepository.save( renewRegistrationRequest );
    }

    public void safeDeleteRenewRegistrationRequests(User user) {
        try {
            List<RenewRegistrationRequest> renewRegistrationRequestList = this.renewRegistrationRequestRepository.findRenewRegistrationRequestByUser( user );
            for (RenewRegistrationRequest renewRegistrationRequest : renewRegistrationRequestList) {
                Files.deleteIfExists( Paths.get( baseConfig.upload, renewRegistrationRequest.getPath(), renewRegistrationRequest.getName()));
                this.renewRegistrationRequestRepository.delete( renewRegistrationRequest );
            }
        } catch (Exception ignored) {
        }
    }


}
