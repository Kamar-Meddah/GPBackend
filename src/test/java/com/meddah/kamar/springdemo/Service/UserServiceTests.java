package com.meddah.kamar.springdemo.Service;

import com.meddah.kamar.springdemo.Exception.UserException;
import com.meddah.kamar.springdemo.Model.User;
import com.meddah.kamar.springdemo.Repository.UserRepository;
import name.falgout.jeffrey.testing.junit.mockito.MockitoExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith({SpringExtension.class, MockitoExtension.class})
@SpringBootTest
@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
public class UserServiceTests {

    private User user;
    private UserRepository userRepository;
    private UserService userService;

    public UserServiceTests(@Autowired UserRepository userRepository, @Autowired UserService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }

    @BeforeAll
    void createUser() throws UserException {
        this.user = new User();
        this.user.setEmail( "azeaze@gmail.com" );
        this.user.setRole( "user" );
        this.user.setUsername( "user" );
        this.user.setFirstName( "azaz" );
        this.user.setLastName( "azaz" );
        this.user.setPassword( "azer" );
    }

    @Test
    public void testCreateUser() {
        User user = this.userService.create(this.user);
        Assertions.assertNull( user, "Create a user" );
    }


}
