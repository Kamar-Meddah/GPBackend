package com.meddah.kamar.springdemo.Service;

import com.meddah.kamar.springdemo.Model.Image;
import com.meddah.kamar.springdemo.Model.Post;
import com.meddah.kamar.springdemo.Model.User;
import com.meddah.kamar.springdemo.Repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.UUID;

@Service
@Transactional

public class PostService {

    private final PostRepository postRepository;
    private final ImageService imageService;

    @Autowired
    public PostService(PostRepository postRepository, ImageService imageService) {
        this.postRepository = postRepository;
        this.imageService = imageService;
    }


    public Post create(String content, User user) {
        Post post = new Post();
        post.setContent( content );
        post.setUser( user );
        return this.postRepository.save( post );
    }

    public Post update(Post post) {
        return this.postRepository.save( post );
    }

    public Page<Post> getAllPosts(int page, int perPage) {
        return this.postRepository.findAll( PageRequest.of( page, perPage, Sort.by( Sort.Direction.DESC, "updatedAt" ) ) );
    }

    public Page<Post> getAllPosts(int page, int perPage, String query, User user) {
        return this.postRepository.findAllByUser( user, PageRequest.of( page, perPage, Sort.by( Sort.Direction.DESC, "updatedAt" ) ) );
    }

    public void deletePost(Post post) throws IOException {
        for (Image image : post.getImages()) {
            this.imageService.deleteImage( image );
        }
        this.postRepository.delete( post );

    }
    @Transactional
    public Post findPost(String id) {
        return this.postRepository.findPostById( UUID.fromString( id ) );
    }
}