package com.meddah.kamar.springdemo.Controller;

import com.meddah.kamar.springdemo.Config.BaseConfig;
import com.meddah.kamar.springdemo.Model.Notification;
import com.meddah.kamar.springdemo.Model.User;
import com.meddah.kamar.springdemo.Security.Annotation.Authenticated;
import com.meddah.kamar.springdemo.Security.auth.AuthFactory;
import com.meddah.kamar.springdemo.Service.NotificationsService;
import com.meddah.kamar.springdemo.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/api/notifications/")
public class NotificationsController {

    private final NotificationsService notificationsService;
    private final UserService userService;
    private final BaseConfig baseConfig;

    @Autowired
    public NotificationsController(NotificationsService notificationsService, UserService userService, BaseConfig baseConfig) {
        this.notificationsService = notificationsService;
        this.userService = userService;
        this.baseConfig = baseConfig;
    }

    @GetMapping
    @Authenticated
    public Page<Notification> getNotifications(HttpServletResponse response, @RequestParam("page") String page) throws IOException {
        int p = Integer.parseInt( page ) - 1;
        if (p >= 0) {
            User user = AuthFactory.getUser();
            return this.notificationsService.getAllNotificationsByUser( user, p, baseConfig.perPage );
        } else {
            response.sendError( 400, "invalid page input" );
            return null;
        }
    }

    @PatchMapping("{id}")
    @Authenticated
    public void setNotificationSeen(HttpServletResponse response,@PathVariable("id") String id) throws IOException {
        User user = AuthFactory.getUser();
        Notification notification = this.notificationsService.getNotificationByIdAndUser( UUID.fromString( id ), user );
        if (notification == null) {
            response.sendError( 404, "Not Found fuck" );
        } else {
            notification.setSeen( true );
            this.notificationsService.setnotificationSeen( notification );
            response.setStatus( 201 );
        }
    }

}
