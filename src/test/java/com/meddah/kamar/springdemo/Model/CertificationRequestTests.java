package com.meddah.kamar.springdemo.Model;

import com.meddah.kamar.springdemo.Exception.CertificationRequestException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Objects;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CertificationRequestTests {

    @Test
    public void testCorrectDate() throws CertificationRequestException {
        CertificationRequest certificationRequest = new CertificationRequest();
        certificationRequest.setDate( "2011" );
        Assertions.assertTrue( Objects.equals( certificationRequest.getDate(), "2011" ), "Does not throw error when date is correct" );
    }

    @Test
    @DisplayName("Thow Exception When Date Is Incorrect")
    public void testThowsExceptionWhenDateIsIncorrect() throws CertificationRequestException {
        CertificationRequest certificationRequest = new CertificationRequest();
        Assertions.assertThrows( CertificationRequestException.class, () -> {
            certificationRequest.setDate( "3000" );
        }, "Throws error when date is invalid" );
    }

}
