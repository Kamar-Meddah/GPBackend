package com.meddah.kamar.springdemo.Controller;

import com.meddah.kamar.springdemo.Config.BaseConfig;
import com.meddah.kamar.springdemo.Exception.MarkException;
import com.meddah.kamar.springdemo.Model.Mark;
import com.meddah.kamar.springdemo.Model.User;
import com.meddah.kamar.springdemo.Security.Annotation.Authenticated;
import com.meddah.kamar.springdemo.Security.Annotation.Student;
import com.meddah.kamar.springdemo.Security.Annotation.Teacher;
import com.meddah.kamar.springdemo.Security.auth.AuthFactory;
import com.meddah.kamar.springdemo.Service.MarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@RequestMapping("/api/marks/")
public class MarkController {

    private final MarkService markService;
    private final BaseConfig baseConfig;

    @Autowired
    public MarkController(MarkService markService, BaseConfig baseConfig) {
        this.markService = markService;
        this.baseConfig = baseConfig;
    }

    @GetMapping
    @Authenticated
    @Student
    public List<Mark> findMarksByParams(HttpServletResponse response,
                                        @RequestParam("semester") String semester,
                                        @RequestParam("schoolYear") String schoolYear,
                                        @RequestParam("grade") String grade,
                                        @RequestParam("type") String type) throws IOException {
        try {
            return this.markService.findMarksBySemesterAndSchoolyearAndGradeAndType( semester, schoolYear, grade, type );
        } catch (Exception exception) {
            response.sendError( 406, "an error occured" );
            return null;
        }
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_PDF_VALUE)
    public byte[] getMark(HttpServletResponse response, @PathVariable("id") String id) throws IOException {
        Mark mark = this.markService.findMark( id );
        if (mark != null) {
            Path path = Paths.get( baseConfig.upload, mark.getPath(), mark.getFileName() );
            InputStream inputStream = Files.newInputStream( path );
            return inputStream.readAllBytes();
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "File not found");
            return null;
        }
    }

    @PostMapping
    @Authenticated
    @Teacher
    public void AddMark(HttpServletResponse response,
                        @RequestParam("file") MultipartFile file,
                        @RequestParam("semester") String semester,
                        @RequestParam("schoolYear") String schoolYear,
                        @RequestParam("name") String name,
                        @RequestParam("grade") String grade,
                        @RequestParam("type") String type) throws IOException {

        try {
            try {
                User user = AuthFactory.getUser();
                Mark mark = this.markService.createMark( schoolYear, semester, name, grade, type, user );
                Path path = Paths.get( baseConfig.upload, mark.getPath(), mark.getFileName() );
                Files.createDirectories( path.getParent() );
                Files.newOutputStream( path ).write( file.getBytes() );
            } catch (MarkException markException) {
                response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, markException.getMessage());
            }
        } catch (Exception exception) {
            response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, "inputs already exist");
        }

    }


}
